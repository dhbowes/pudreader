/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pudreader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 *
 * @author comqdhb
 */
public class ZipReader implements ParrotReader {

    private ArrayList<String> objects;
    private ArrayList<DataPoint> data;
    private TreeMap<String, String> headerMap = new TreeMap<>();
    private static final String ENCODING = "UTF-8";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        String name="tyson";
        String file = "../"+name+".json.zip";
        ZipReader pud = new ZipReader(file);
        System.out.println("date is \t" + pud.date);
        System.out.println("controller is \t" + pud.controller);
        System.out.println("app is \t\t" + pud.app);
        System.out.println("block size is \t" + pud.blockSize);
        /*
        pud.printHeader();
        for (DataPoint dp : pud.data) {
            System.out.println(dp);
        }
        System.out.println(KMLWriter.toString(pud,"product"));
        */
        KMLWriter.toFile("../"+name+".kml",pud,"product");
        //System.out.println(GPXWriter.toString(pud,"product"));
        GPXWriter.toFile("../"+name+".gpx",pud,"product");
        CSVWriter.toFile("../"+name+".csv",pud,"product");
    }

    private String date;
    private String controller;
    private String app;
    private String sn;
    private int blockSize;

    public ZipReader(String fileName) throws FileNotFoundException, IOException {
     File f =new File(fileName);
     loadFile(f);
    }
     public ZipReader(File f) throws FileNotFoundException, IOException {
    loadFile(f);}
     
        
     private void loadFile(File f) throws FileNotFoundException, IOException{ 
        ZipInputStream zis = new ZipInputStream(new FileInputStream(f));
        ZipFile zf = new ZipFile(f);
        Enumeration<? extends ZipEntry> e = zf.entries();
        while (e.hasMoreElements()) {
            ZipEntry ze = e.nextElement();
            InputStream is = zf.getInputStream(ze);
            loadData(is);
        }
    }

    public ZipReader(InputStream is) throws IOException {
        loadData(is);
    }

    private void loadData(InputStream is) throws IOException {

        objects = new ArrayList<>();
        data = new ArrayList<>();

        JSONTokener tok = new JSONTokener(is);

        if (tok.more()) {
            Object ob = tok.nextValue();
            JSONObject o = (JSONObject) ob;
            date = o.getString("date");
            controller = o.getString("controller_model");
            app = o.getString("controller_application");
            sn = o.getString("serial_number");
            for (String k : o.keySet()) {
                if ("details_headers".equals(k)) {
                    JSONArray children = o.getJSONArray("details_headers");
                    System.out.println("" + children);
                    for (Object x : children) {
                        System.out.println("x=" + x);
                        objects.add(x + "");
                    }
                } else if ("details_data".equals(k)) {
                    JSONArray children = o.getJSONArray("details_data");
                    for (Object a : children) {
                        JSONArray ca = (JSONArray) a;
                        DataPoint dp = new DataPoint(objects, ca);
                        data.add(dp);
                    }
                } else {
                    headerMap.put(k, "" + o.get(k));
                }
            }
        }

        is.close();
    }

    public void printHeader() {
        for (Map.Entry<String, String> keyPair : headerMap.entrySet()) {
            System.out.println(keyPair.getKey() + "\t" + keyPair.getValue());
        }
    }

    /**
     * @return the data
     */
    public ArrayList<DataPoint> getDataPoints() {
        return data;
    }

    /**
     * @return the headerMap
     */
    public TreeMap<String, String> getHeaders() {
        return headerMap;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @return the controller
     */
    public String getController() {
        return controller;
    }

    /**
     * @return the app
     */
    public String getApp() {
        return app;
    }

    /**
     * @return the serial Number
     */
    public String getSerialNumber() {
        return sn;
    }

    /**
     * @return the data
     */
    public ArrayList<DataPoint> getData() {
        return data;
    }

    @Override
    public List<String> getDataHeaders() {
      return objects;
    }

}
