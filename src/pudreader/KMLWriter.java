/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pudreader;

import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author comqdhb
 */
public class KMLWriter {
    
    private static final String who="controller";//controller | product
    
    public static String toString(ParrotReader pud){
    return toString(pud,who);
    }
    
    public static String toString(ParrotReader pud,String who){
        StringBuilder builder=new StringBuilder();
        builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
"<kml xmlns=\"http://www.opengis.net/kml/2.2\">\n" +
"  <Document>\n" +
"    <name>Paths</name>\n" +
"    <description>Flight of drone on "+pud.getDate()+" using "+pud.getApp() +" on "+pud.getController()+ "</description>\n" +
"    <Style id=\"yellowLineGreenPoly\">\n" +
"      <LineStyle>\n" +
"        <color>7f00ffff</color>\n" +
"        <width>4</width>\n" +
"      </LineStyle>\n" +
"      <PolyStyle>\n" +
"        <color>7f00ff00</color>\n" +
"      </PolyStyle>\n" +
"    </Style>\n" +
"    <Placemark>\n" +
"      <name>Absolute Extruded</name>\n" +
"      <description>Transparent green wall with yellow outlines</description>\n" +
"      <styleUrl>#yellowLineGreenPoly</styleUrl>\n" +
"      <LineString>\n" +
"        <extrude>1</extrude>\n" +
"        <tessellate>1</tessellate>\n" +
"        <altitudeMode>absolute</altitudeMode>\n" +
"        <coordinates>\n");
        for (DataPoint p:pud.getData()){
            float lon= p.getDataValue(who+"_gps_longitude").value.floatValue();
            float lat= p.getDataValue(who+"_gps_latitude").value.floatValue();
            if (lat<90 && lat>-90 && lon<180 && lon>-180){
            builder.append("          ");
            builder.append(lon).append(",");
            builder.append(lat).append(","+
                    (1000+p.getDataValue("altitude").value.floatValue()/1000)
                    +"\n");
            }
        }
        builder.append(
"        </coordinates>\n" +
"      </LineString>\n" +
"    </Placemark>\n" +
"  </Document>\n" +
"</kml>");
        return builder.toString();
    }

    public static void toFile(String datakml, ParrotReader pud, String product) {
    
        try {
            FileWriter fw = new FileWriter(datakml);
            fw.append(toString(pud, product));
            fw.flush();
            fw.close();
        } catch (IOException i) {
            i.printStackTrace();
        }
        
    }
}
