/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pudreader;

import java.text.DecimalFormat;

/**
 *
 * @author comqdhb
 */
public class DataValue {
        public final String name;
        public final int len;
        public final String type;
        public final Number value;

        DataValue(String name, int len, String type, Number value) {
            this.name=name;
            this.len=len;
            this.type=type;
            this.value=value;
        }
        
        public String toString(){
            String r=""+value;
            if (type.equals("float") || type.equals("double")){
                r=new DecimalFormat(" ###0.00000;-###0.00000").format(value);
            }
            return name+"("+type+":"+len+")="+r;
        }

    }
/*
Name                            Type	Size	Description
time                            integer	4	Timestamp of the log entry, in milliseconds
battery_level                   integer	4	Battery level, in percent
controller_gps_longitude        double	8	Controller GPS longitude, in degrees
controller_gps_latitude         double	8	Controller GPS latitude, in degrees
flying_state                    integer	1	Flying state: 1 = landed, 2 = in the air, 3 = in the air
alert_state                     integer	1	Alert state: 0 = normal
wifi_signal                     integer	1	WiFi signal strength, always 0 right now
product_gps_available           boolean	1	Bebop GPS availability, always 0 right now
product_gps_longitude           double	8	BeBop GPS longitude, in degrees
product_gps_latitude            double	8	BeBop GPS latitude, in degrees
product_gps_position_error	integer	4	BeBop GPS position error, always 0 right now
speed_vx                        float	4	Horizontal speed, unknown units
speed_vy                        float	4	Horizontal speed, unknown units
speed_vz                        float	4	Vertical speed, unknown units
angle_phi                       float	4	Euler angle phi, likely in radians
angle_theta                     float	4	Euler angle theta, likely in radians
angle_psi                       float	4	Euler angle psi, likely in radians
altitude                        integer	4	Altitude, likely in centimeters
flip_type                       integer	1	Flip type, 0 = no flip
*/
