/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pudreader;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

/**
 *
 * @author comqdhb
 */
public class CSVWriter {

    private static final String who = "controller";//controller | product

    public static String toString(ParrotReader pud) {
        return toString(pud, who);
    }

    public static String toString(ParrotReader pud, String who) {
        StringBuilder b = new StringBuilder();
        //2015-12-30T113913+0000
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'kkmmssz");//E MM dd kk:mm:ss z yyyy");
        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd'T'kk:mm:ss.SSSz");//E MM dd kk:mm:ss z yyyy");

        Date result = null;
        try {
            result = df.parse(pud.getDate());
        } catch (ParseException pe) {
            pe.printStackTrace();
        }

        Iterator<DataPoint> it = pud.getData().iterator();
        boolean start = true;
        for (String x : pud.getDataHeaders()) {
            b.append(start ? "" : ",").append(x);
            start = false;
        }
        b.append("\n");

        while (it.hasNext()) {

            DataPoint p = it.next();
            Date d = new Date(result.getTime() + p.getDataValue("time").value.longValue());

            start = true;
            for (String x : pud.getDataHeaders()) {
                b.append(start ? "" : ",");
                if (p.getDataValue(x)!=null)b.append(p.getDataValue(x).value);
                start = false;
            }
            b.append("," + df2.format(d) + "\n");

        }
        
        return b.toString();
    }

    public static void toFile(String datakml, ParrotReader pud, String product) {

        try {
            FileWriter fw = new FileWriter(datakml);
            fw.append(toString(pud, product));
            fw.flush();
            fw.close();
        } catch (IOException i) {
            i.printStackTrace();
        }

    }

}
