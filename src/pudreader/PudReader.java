/*
 * see https://github.com/cucx/bebop for pud format
 */
package pudreader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 *
 * @author comqdhb
 */
public class PudReader implements ParrotReader {

    private ArrayList<JSONObject> objects;
    private ArrayList<DataPoint> data;
    private TreeMap<String, String> headerMap = new TreeMap<>();
    private static final String ENCODING = "UTF-8";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        String file = "0907_2015-12-30T113913+0000_13C3C1.pud";
        PudReader pud = new PudReader(file);
        System.out.println("date is \t" + pud.date);
        System.out.println("controller is \t" + pud.controller);
        System.out.println("app is \t\t" + pud.app);
        System.out.println("block size is \t" + pud.blockSize);
        pud.printHeader();
        for (DataPoint dp : pud.data) {
            System.out.println(dp);
        }
        System.out.println(KMLWriter.toString(pud));
        System.out.println(GPXWriter.toString(pud));
    }

    private String date;
    private String controller;
    private String app;
    private String sn;
    private int blockSize;

    public PudReader(String fileName) throws FileNotFoundException, IOException {
        File f = new File(fileName);
        InputStream is = new FileInputStream(f);
        loadData(is);
    }

    public PudReader(InputStream is) throws IOException {
        loadData(is);
    }

    private void loadData(InputStream is) throws IOException {

        objects = new ArrayList<>();
        data = new ArrayList<>();

        blockSize = 0;
        String jsonText = getNextString(is);

        JSONTokener tok = new JSONTokener(jsonText);

        if (tok.more()) {
            Object ob = tok.nextValue();
            JSONObject o = (JSONObject) ob;
            date = o.getString("date");
            controller = o.getString("controller_model");
            app = o.getString("controller_application");
            sn = o.getString("serial_number");
            for (String k : o.keySet()) {
                if ("details_headers".equals(k)) {
                    JSONArray children = o.getJSONArray("details_headers");
                    for (Object x : children) {
                        JSONObject jo = (JSONObject) x;
                        objects.add(jo);
                        blockSize += jo.getInt("size");
                    }
                } else {
                    headerMap.put(k, "" + o.get(k));
                }
            }

            byte[] bytes = new byte[blockSize];
            while ((is.read(bytes)) > -1) {
                is.read(bytes);
                DataPoint dp = new DataPoint(objects, bytes);
                data.add(dp);
            }
        }
        is.close();
    }

    /**
     *
     * @param is
     * @return
     */
    private String getNextString(InputStream is) {
        StringBuilder sb = new StringBuilder();
        try {
            int b;
            while ((b = is.read()) > 0) {
                sb.append((char) b);
            }
        } catch (IOException iO) {
            iO.printStackTrace();
        }
        return sb.toString();
    }

    /**
     * NB this method will consume the entire input stream!!!
     *
     * @param in
     * @return
     */
    private String getNilTerminatedString(InputStream in) {
        String result = "";
        Scanner scanner = new Scanner(in, ENCODING);
        scanner.useDelimiter("\u0000");
        if (scanner.hasNext()) {
            result = scanner.next();
        }
        return result;
    }

    @Override
    public void printHeader() {
        for (Map.Entry<String, String> keyPair : headerMap.entrySet()) {
            System.out.println(keyPair.getKey() + "\t" + keyPair.getValue());
        }
    }

    /**
     * @return the data
     */
    @Override
    public ArrayList<DataPoint> getDataPoints() {
        return data;
    }

    /**
     * @return the headerMap
     */
    @Override
    public TreeMap<String, String> getHeaders() {
        return headerMap;
    }

    /**
     * @return the date
     */
    @Override
    public String getDate() {
        return date;
    }

    /**
     * @return the controller
     */
    @Override
    public String getController() {
        return controller;
    }

    /**
     * @return the app
     */
    @Override
    public String getApp() {
        return app;
    }

    /**
     * @return the serial Number
     */
    @Override
    public String getSerialNumber() {
        return sn;
    }

    /**
     * @return the data
     */
    @Override
    public ArrayList<DataPoint> getData() {
        return data;
    }

    List<String> obs=null;
    @Override
    public List<String> getDataHeaders() {
        if(obs==null){
            obs=new ArrayList<String>();
            for (JSONObject jo:objects){
               obs.add(jo.getString("name"));
            }
        }
        return obs;
    }

}
