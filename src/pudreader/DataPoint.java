/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pudreader;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author comqdhb
 */
public class DataPoint extends ArrayList<DataValue> {

    public DataPoint(ArrayList<JSONObject> objects, byte[] bytes) {
        parse(objects,bytes);
    }

    DataPoint(ArrayList<String> objects, JSONArray ca) {
        for (int i=0;i<objects.size();i++){
            String nm=objects.get(i);
            
            if ("product_gps_available".equals(nm)){
                boolean tf=ca.getBoolean(i);
                //add(new DataValue(nm,0,"tf",tf));
            }else {
                Number value=ca.getDouble(i);
                add(new DataValue(nm,0,"d",value));
            }
        }
    }
    private void parse(ArrayList<JSONObject> objects, byte[] bytes){
        ByteBuffer wrapped = ByteBuffer.wrap(bytes); // big-endian by default
        wrapped.order(ByteOrder.LITTLE_ENDIAN);
        
        for (JSONObject o : objects) {
            String name = o.getString("name");
            int len = o.getInt("size");
            String type = o.getString("type");
            Number value=getNextNumber(wrapped,type,len);
            add(new DataValue(name,len,type,value));
        }
        
    }
    
    private Number getNextNumber(ByteBuffer wrapped,String type,int len){
        Number value=null;
            switch (type) {
                case "integer":
                    switch (len){
                        case 1:value=wrapped.get();break;
                        case 4:value=wrapped.getInt();break;
                    }
                    break;
                case "double":
                    value=wrapped.getDouble();
                    break;
                case "float":
                    value=wrapped.getFloat();
                    break;
            }
            return value;
    }
    
    public DataValue getDataValue(String name){
        DataValue result=null;
        for (DataValue dv:this){
            if (dv.name.equals(name)){
                result=dv;
            }
        }
        return result;
    }

    
    
    public String toString(){
        String result="";
        for (DataValue x:this){
            result+=x+"\t";
        }
        return result;
    }

}
