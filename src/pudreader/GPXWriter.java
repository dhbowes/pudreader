/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 angle_theta=pitch
 angle_phi=roll
 angle_psi=heading
 */
package pudreader;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

/**
 *
 * @author comqdhb
 */
public class GPXWriter {

    private static final String who = "controller";//controller | product
    private static boolean allowExtensions = true;

    public static String toString(ParrotReader pud) {
        return toString(pud, who);
    }

    public static void toFile(String datakml, ParrotReader pud, String product) {

        try {
            FileWriter fw = new FileWriter(datakml);
            fw.append(toString(pud, product));
            fw.flush();
            fw.close();
        } catch (IOException i) {
            i.printStackTrace();
        }

    }

    public static String toString(ParrotReader pud, String who) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'kkmmssz");//E MM dd kk:mm:ss z yyyy");
        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd'T'kk:mm:ss'Z'");//E MM dd kk:mm:ss z yyyy");
        Date result = null;
        try {
            result = df.parse(pud.getDate());
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        String lastTime = "", thisTime = "";
        StringBuilder w = new StringBuilder();

        StringBuilder b = new StringBuilder();
        Iterator<DataPoint> it = pud.getData().iterator();
        while (it.hasNext()) {

            long et = 0;
            while (it.hasNext()) {

                DataPoint p = it.next();
                long time = p.getDataValue("time").value.longValue();
                long offset = time % 1000;
                Date d = new Date(result.getTime() + p.getDataValue("time").value.longValue());
                thisTime = df2.format(d);
                if (!thisTime.equals(lastTime)) {
                    lastTime = thisTime;
                    float lon = p.getDataValue(who + "_gps_longitude").value.floatValue();
                    float lat = p.getDataValue(who + "_gps_latitude").value.floatValue();
                    if (lat < 90 && lat > -90 && lon < 180 && lon > -180) {
                        b.append("       <trkpt lat=\"" + lat + "\" lon=\"" + lon + "\">\n"
                                + "        <ele>" + p.getDataValue("altitude").value.floatValue() / 1000 + "</ele>\n"
                                + "        <time>" + thisTime + "</time>\n");
                        //b.append( "        <course>"+ calcCourse(p.getDataValue("angle_psi").value.doubleValue())+"\n");
                        if (allowExtensions) {
                            double[] xyz = computeAcceleration(p.getDataValue("angle_theta").value.doubleValue(), p.getDataValue("angle_phi").value.doubleValue());
                            b.append("        <extensions>\n");
                            b.append("          <gpxtpx:TrackPointExtension>\n");
                            b.append("              <gpxtpx:hr>" + p.getDataValue("battery_level").value.intValue() + "</gpxtpx:hr>\n");
                            b.append("              <gpxtpx:cad>" + p.getDataValue("speed").value.intValue() + "</gpxtpx:cad>\n");
                            b.append("              <gpxtpx:course>\"+ calcCourse(p.getDataValue(\"angle_psi\").value.doubleValue())+\"</gpxtpx:course>\n");
                            b.append("              <gpxtpx:bearing>\"+ calcCourse(p.getDataValue(\"angle_psi\").value.doubleValue())+\"</gpxtpx:bearing>\n");
                            
                            b.append("          </gpxtpx:TrackPointExtension>"
                                    + "<acc:AccelerationExtension xmlns:acc=\"http://www.garmin.com/xmlschemas/AccelerationExtension/v1\">\n"
                                    + "            <acc:accel offset=\"" + offset + "\" x=\"" + xyz[0] + "\" y=\"" + xyz[1] + "\" z=\"" + xyz[2] + "\" />"
                                    + "</acc:AccelerationExtension>\n");
                            b.append("         </extensions>\n");
                        }
                        b.append("      </trkpt>\n");
                    }

                    /*
                     <xsd:element name="atemp" type="DegreesCelsius_t" minOccurs="0"/> double
                     <xsd:element name="wtemp" type="DegreesCelsius_t" minOccurs="0"/> double
                     <xsd:element name="depth" type="Meters_t" minOccurs="0"/> double
                     <xsd:element name="hr" type="BeatsPerMinute_t" minOccurs="0"/> unsigned byte
                     <xsd:element name="cad" type="RevolutionsPerMinute_t" minOccurs="0"/> unsigned byte
                     */
                    w.append("<wpt lat=\"" + lat + "\" lon=\"" + lon + "\">\n"
                            + "  <ele>" + p.getDataValue("altitude").value.floatValue() / 1000 + "</ele>\n"
                            + "  <name>020</name>\n"
                            + "  <cmt>020</cmt>\n"
                            + "  <desc>020</desc>\n"
                            + "  <sym>Flag</sym>\n"
                            + "</wpt>\n");
                }
            }
        }

        String gpx = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n"
                + "\n"
                + "<gpx creator=\"Garmin Desktop App\" version=\"1.1\" \n"
                + "     xsi:schemaLocation=\"\n"
                + "http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd \n"
                + "http://www.garmin.com/xmlschemas/WaypointExtension/v1 http://www8.garmin.com/xmlschemas/WaypointExtensionv1.xsd \n"
                + "http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd \n"
                + "http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www8.garmin.com/xmlschemas/GpxExtensionsv3.xsd \n"
                + "http://www.garmin.com/xmlschemas/ActivityExtension/v1 http://www8.garmin.com/xmlschemas/ActivityExtensionv1.xsd \n"
                + "http://www.garmin.com/xmlschemas/AdventuresExtensions/v1 http://www8.garmin.com/xmlschemas/AdventuresExtensionv1.xsd \n"
                + "http://www.garmin.com/xmlschemas/PressureExtension/v1 http://www.garmin.com/xmlschemas/PressureExtensionv1.xsd \n"
                + "http://www.garmin.com/xmlschemas/TripExtensions/v1 http://www.garmin.com/xmlschemas/TripExtensionsv1.xsd \n"
                + "http://www.garmin.com/xmlschemas/TripMetaDataExtensions/v1 http://www.garmin.com/xmlschemas/TripMetaDataExtensionsv1.xsd \n"
                + "http://www.garmin.com/xmlschemas/ViaPointTransportationModeExtensions/v1 http://www.garmin.com/xmlschemas/ViaPointTransportationModeExtensionsv1.xsd \n"
                + "http://www.garmin.com/xmlschemas/CreationTimeExtension/v1 http://www.garmin.com/xmlschemas/CreationTimeExtensionsv1.xsd \n"
                + "http://www.garmin.com/xmlschemas/AccelerationExtension/v1 http://www.garmin.com/xmlschemas/AccelerationExtensionv1.xsd \n"
                + "http://www.garmin.com/xmlschemas/PowerExtension/v1 http://www.garmin.com/xmlschemas/PowerExtensionv1.xsd \n"
                + "http://www.garmin.com/xmlschemas/VideoExtension/v1 http://www.garmin.com/xmlschemas/VideoExtensionv1.xsd\" \n"
                + "     xmlns=\"http://www.topografix.com/GPX/1/1\" \n"
                + "     xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \n"
                + "     xmlns:wptx1=\"http://www.garmin.com/xmlschemas/WaypointExtension/v1\" \n"
                + "     xmlns:gpxtrx=\"http://www.garmin.com/xmlschemas/GpxExtensions/v3\" \n"
                + "     xmlns:gpxtpx=\"http://www.garmin.com/xmlschemas/TrackPointExtension/v1\" \n"
                + "     xmlns:gpxx=\"http://www.garmin.com/xmlschemas/GpxExtensions/v3\" \n"
                + "     xmlns:trp=\"http://www.garmin.com/xmlschemas/TripExtensions/v1\" \n"
                + "     xmlns:adv=\"http://www.garmin.com/xmlschemas/AdventuresExtensions/v1\" \n"
                + "     xmlns:prs=\"http://www.garmin.com/xmlschemas/PressureExtension/v1\" \n"
                + "     xmlns:tmd=\"http://www.garmin.com/xmlschemas/TripMetaDataExtensions/v1\" \n"
                + "     xmlns:vptm=\"http://www.garmin.com/xmlschemas/ViaPointTransportationModeExtensions/v1\" \n"
                + "     xmlns:ctx=\"http://www.garmin.com/xmlschemas/CreationTimeExtension/v1\" \n"
                + "     xmlns:gpxacc=\"http://www.garmin.com/xmlschemas/AccelerationExtension/v1\" \n"
                + "     xmlns:gpxpx=\"http://www.garmin.com/xmlschemas/PowerExtension/v1\" \n"
                + "     xmlns:vidx1=\"http://www.garmin.com/xmlschemas/VideoExtension/v1\">\n"
                + "  <metadata>\n"
                + "    <link href=\"http://www.garmin.com\">\n"
                + "      <text>Garmin International</text>\n"
                + "    </link>\n"
                + //"    <time>2009-10-17T22:58:43Z</time>\n" +
                "    <time>" + df2.format(result) + "</time>\n"
                + "  </metadata>\n"
                + "  <trk>\n"
                + "    <name>Example GPX Document</name>\n"
                + "    <trkseg>\n"
                + "     " + b.toString()
                + "    </trkseg>\n"
                + "  </trk>\n"
                + "</gpx>";
        return gpx;
    }

    /**
     * PRE-CONDITION ||pitch and roll|| < Pi/2
     *
     * @param pitch angle in radians
     * @param roll angle in radians
     * @return acceleration in xyz axis
     */
    public static double[] computeAcceleration(double pitch, double roll) {
        double[] xyz = new double[3];
        double tp, tr;
        double ninety = Math.PI / 2;
        tp = Math.tan(ninety - pitch);
        tr = Math.tan(ninety - roll);
        double z2 = 1 + 1 / tp / tp + 1 / tr / tr;
        z2 = 1 / z2;
        double y2 = z2 / tp / tp;
        double x2 = z2 / tr / tr;
        xyz[2] = -Math.sqrt(z2);
        xyz[1] = (pitch > 0 ? -1 : 1) * Math.sqrt(y2);
        xyz[0] = (roll > 0 ? -1 : 1) * Math.sqrt(x2);
        return xyz;
    }

    private static String calcCourse(double r) {
      double c=r/Math.PI*180;
      while (c>360){
          c-=360;
      }
      while (c<0){
          c+=360;
      }
      return ""+c;
    }
}
