/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pudreader;

import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author comqdhb
 */
public class Main {

    public static void main(String[] args) throws IOException {
        if (args.length > 0) {
            String file = args[0];
            PudReader pud = new PudReader(file);
            for (int i = 1; i < args.length; i++) {
                action(args[i], pud);
            }
        } else {
            printHelp();
        }
    }

    private static void printHelp() {
        System.out.println("The first argument should be the name of the file\n"
                + "\t followed by -v[gk] verbose with g=gpx k=kml,\n "
                + "\t -k:<filename>, -g:<filename> to export to either or kml or GPX");
    }

    private static void action(String arg, PudReader pud) throws IOException {
        if (arg.startsWith("-v")) {
            pud.printHeader();
            for (DataPoint dp : pud.getData()) {
                System.out.println(dp);
            }
            if (arg.contains("k")) {
                System.out.println(KMLWriter.toString(pud));
            }
            if (arg.contains("g")) {
                System.out.println(GPXWriter.toString(pud));
            }
        }
        if (arg.startsWith("-k:")) {
            String fileName = arg.substring(3);
            FileWriter fw = new FileWriter(fileName);
            fw.append(KMLWriter.toString(pud));
            fw.flush();
            fw.close();
        }
        if (arg.startsWith("-g:")) {
            String fileName = arg.substring(3);
            FileWriter fw = new FileWriter(fileName);
            fw.append(GPXWriter.toString(pud));
            fw.flush();
            fw.close();
        }
    }

}
