/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pudreader;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 *
 * @author comqdhb
 */
public interface ParrotReader {

    /**
     * @return the app
     */
    String getApp();

    /**
     * @return the controller
     */
    String getController();

    /**
     * @return the data
     */
    ArrayList<DataPoint> getData();

    /**
     * @return the data
     */
    ArrayList<DataPoint> getDataPoints();

    /**
     * @return the date
     */
    String getDate();

    /**
     * @return the headerMap
     */
    TreeMap<String, String> getHeaders();

    /**
     * @return the serial Number
     */
    String getSerialNumber();

    void printHeader();
    
    List<String> getDataHeaders();
    
}
