/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit;

/**
 *
 * @author comqdhb
 * 
Templates for Garmin Virb Edit
If you would like to use the GPS overlay templates created especially for Bebop drone, then just click on the button "Install Garmin Virb GPS overlay Templates to PC" in the FlightData Manager tool, and select if you want the templates using meters and km/h or the templates using mph and feet.

In Garmin Virb Edit you should select one of the 6 templates called 'Bebop Template 1' to 'Bebop Template 7', see screendump of these 6 templates below.

If you instead use on of the default templates in Garmin Virb, a whole lot of the data will not be shown correctly, unless you really know what you are doing.

If you decide to create your own template you need the following info (but if you just use one of the two Bebop templates you can just ignore the following):
In order to make it independently of the units selected in the Garmin Virb settings menu, speed, altitude and distance is not reported as speed, altitude and distance in the Garmin Fit file, which would give a lot of problems if the user have made a wrong setting.
Instead the various data is reported the following way in the Garmin Fit file:
'Speed' is reported as 'Stance Time' in Fit file (is found in Garmin Virb under Gauges -> All Running)
'Altitude' for text is reported as 'Stance Time Percent' in Fit file (is found in Garmin Virb under Gauges -> All Running)
'Altitude' for graph is reported 'Elevation' in Fit file (is found in Garmin Virb under Gauges -> All Position)
'Drone distance' is reported as 'Engine RPM' in Fit file (is found in Garmin Virb under Gauges -> All OBD-II Sensors)
'Distance flown' is reported as 'Power' in Fit file (is found in Garmin Virb under Gauges -> All Sensors)
'Battery %' is reported as 'Cadance' in Fit file (is found in Garmin Virb under Gauges -> All Sensors)
'Orientation of drone' is reported as 'Aviation Track Angle' in Fit file (is found in Garmin Virb under Gauges -> All Aviation)
'Pitch' and 'Roll' is reported as 'Pitch' and 'Roll' in Fit file (is found in Garmin Virb under Gauges -> All Aviation)
'WiFi signal level' (only for Bebop2) is reported as 'HeartRate' in Fit file (is found in Garmin Virb under Gauges -> Sensors -> Heart Rate)
'Number of GPS satellites' (only for Bebop2) is reported as 'verticalOscillation' in Fit file (is found in Garmin Virb under Gauges -> Running -> Vertical Oscilation)
 */
public class FitWriter {
    
}
