# README #



### PudReader ###

* Utility to read pud files from parrot drones
* Version 0.01

### How do I get set up? ###

* git clone https://bitbucket.org/dhbowes/pudreader.git
* cd PudRead
* ant jar
* java -jar "dist/PudReader.jar" "<filename.pud>" -v -k:a.kml -g:b.gpx



### Who do I talk to? ###

* Repo owner or admin: David Bowes